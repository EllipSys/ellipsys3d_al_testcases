# ellipsys3d_al_testcases
**Actuator Line test cases**

There four cases that cover most functions of the model. A simple box grid is 
used for all of them and the velocity components are tested.

## aero_forces
Single turbine with aerofoil data and blade defintion, without any tip or 
smearing correction. 

## aero_forces_kaya
Furhter to the preceding test it runs the simulation with the smearing
correction switched on. 

## prescribed_forces
Tests the AL with prescribed force distribution.

## multi_aero_forces_kaya
As aero_forces_kaya, but with two turbines. 